function mySecondCache(key, value, seconds) {
	/**
	 * @param key
	 * @param value 缓存值
	 * @param seconds 保存时间 单位秒
	 */
	let nowTime = Date.parse(new Date()) / 1000;
	if (key && value) {
		let expire = nowTime + Number(seconds);
		uni.setStorageSync(key,JSON.stringify(value) + '|' +expire)
	} else if (key && !value) {
		let val = uni.getStorageSync(key);
		if (val) {
			// 缓存存在，判断是否过期
			let temp = val.split('|')
			if (!temp[1] || temp[1] <= nowTime) {
				uni.removeStorageSync(key)
				return '';
			} else {
				return JSON.parse(temp[0]);
			}
		}
	}
}
	

function myTimeCache(key, value, timestamp) {
	/**
	 * @param key
	 * @param value 缓存值
	 * @param timestamp 时间戳
	 */
	if(timestamp==null){
		timestamp = new Date()
	}
	if (key && value) {
		uni.setStorageSync(key,JSON.stringify(value) + '||=||' +timestamp)
	} else if (key && !value) {
		let val = uni.getStorageSync(key);
		if (val) {
			// 缓存存在，判断是否过期
			let temp = val.split('||=||')
			if (!temp[1] || temp[1] <= new Date()) {
				uni.removeStorageSync(key)
				return '';
			} else {
				return JSON.parse(temp[0]);
			}
		}
	}
}

export  {
mySecondCache,
myTimeCache
}